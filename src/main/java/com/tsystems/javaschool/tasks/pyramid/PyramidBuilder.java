package com.tsystems.javaschool.tasks.pyramid;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

public class PyramidBuilder {

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
    public int[][] buildPyramid(List<Integer> inputNumbers) {
        try {
            inputNumbers.toArray();
        } catch (OutOfMemoryError E) {
            throw new CannotBuildPyramidException();
        }

        if (inputNumbers.contains(null)) {
            throw new CannotBuildPyramidException();
        } else {
            inputNumbers.sort((o1, o2) -> (o1 > o2) ? 1 : (o1 < o2) ? -1 : 0);

            int inputSize = inputNumbers.size();
            int[][] pyramid;
            if (isAbleToBuildPyramid(inputSize)) {
                pyramid = new int[pyramidScale(inputSize)][pyramidScale(inputSize) * 2 - 1];
                for (int i = 0; i < pyramid.length; ++i) {
                    for (int j = 0; j < pyramid[0].length; ++j) {
                        pyramid[i][j] = 0;
                    }
                }
                int k = 0;
                int width = pyramidScale(inputSize);
                for (int i = 0; i <= width - 1; ++i) {
                    for (int j = width - 1 - i; j <= width - 1 + i; j += 2, ++k) {
                        pyramid[i][j] = inputNumbers.get(k);
                    }
                }
                return pyramid;
            } else {
                throw new CannotBuildPyramidException();
            }
        }
    }

    public static boolean isAbleToBuildPyramid(int massLength) {
        return Math.sqrt(1 + 8 * massLength) % 1 == 0;
    }

    public static int pyramidScale(int massLength) {
        return (int)((-1 + Math.sqrt(1 + 8 * massLength)) / 2);
    }
}
