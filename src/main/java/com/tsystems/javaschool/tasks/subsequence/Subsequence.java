package com.tsystems.javaschool.tasks.subsequence;

import java.util.List;
import java.util.stream.Stream;

import static java.util.stream.Collectors.toList;

public class Subsequence {

    /**
     * Checks if it is possible to get a sequence which is equal to the first
     * one by removing some elements from the second one.
     *
     * @param x first sequence
     * @param y second sequence
     * @return <code>true</code> if possible, otherwise <code>false</code>
     */
    @SuppressWarnings("rawtypes")
    public boolean find(List x, List y) {
        if(x == null || y == null || x.contains(null) || y.contains(null)) {
            throw new IllegalArgumentException();
        }
        int indexOfLastCoincidence = 0;
        int sumOfCurrentCoincidences = 0;
        for (int i = 0; i < x.size(); ++i) {
            for (int j = indexOfLastCoincidence; j < y.size(); ++j) {
                if(x.get(i).equals(y.get(j))) {
                    indexOfLastCoincidence = j;
                    ++sumOfCurrentCoincidences;
                    break;
                }
            }
        }
        return sumOfCurrentCoincidences == x.size();
    }
}
